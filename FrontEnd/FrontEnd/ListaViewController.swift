//
//  ListaViewController.swift
//  FrontEnd
//
//  Created by Juan Erazo on 8/6/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class ListaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    var usuarios  = [Persona]()
    
    @IBOutlet weak var tablaUsuarios: UITableView!
    

    override func viewWillAppear(_ animated: Bool) {
//        nombresUsuarios.append("Nombre \(n)")
//        apellidosUsuarios.append("Apellido \(n)")
//        cedulasUsuarios.append("Cedula \(n)")
//        n = n + 1
        //actualizacion desde el backend
        NotificationCenter.default.addObserver(self, selector:#selector(actualizarInformacion), name: NSNotification.Name("actualizar"), object: nil)
        

    }
    
    func actualizarInformacion(_ notification: Notification){
        print("actualizandoo ....")
        usuarios = notification.userInfo?["listo"] as! [Persona]
        tablaUsuarios.reloadData()
       
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("actualizar"),object:nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let backend = BackEndManager()
        print("consultando datos")
        backend.consultarDatos()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return usuarios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celdaUsuario") as! UsuariosTableViewCell
        
        cell.nombreLabel.text = usuarios[indexPath.row].nombre
        cell.apellidoLabel.text = usuarios[indexPath.row].apellido
        cell.cedulaLabel.text = usuarios[indexPath.row].cedula
        
        return cell
        
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
