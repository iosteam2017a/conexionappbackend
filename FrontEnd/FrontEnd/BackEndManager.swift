//
//  BackEndManager.swift
//  FrontEnd
//
//  Created by Juan Erazo on 25/5/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit
import Alamofire

class BackEndManager: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

//    
//    let urlString = "http://localhost:1337/Usuario/create?nombre=\(txtNombre!.text!)&apellido=\(txtApellidp!.text!)"
//    //        let urlString = "http://localhost:1337/Usuario/1"
//    
//    print("aaa:\(txtApellidp!.text!)")
//    
//    //Alamofire.request(urlString)
//    
//    Alamofire.request(urlString).responseJSON { response in
//    print(response.request)  // original URL request
//    print(response.response) // HTTP URL response
//    print(response.data)     // server data
//    print(response.result)   // result of response serialization
//    
//    if let JSON = response.result.value {
//    print("JSON: \(JSON)")
//    }
//    }
    
    func  solicitarBackEnd(urlString:String){
            Alamofire.request(urlString).responseJSON { response in
                
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
        
            }

            }
  
    }
    
    
    
    func consultarDatos(){
        
        Alamofire.request("http://localhost:1337/Usuario/find").responseJSON { response in
            
            if let JSON = response.result.value {
                
                let jsonDict = JSON as! NSArray;
                var array = [Persona]()
                
                
                for i in jsonDict {
                    let dict = i as! NSDictionary
                    
                    array.append(Persona(nombre:dict["nombre"]! as! String,apellido:dict["apellido"]! as! String,cedula:dict["cedula"]! as! String))
                
                    
                }
                
                
                NotificationCenter.default.post(name: NSNotification.Name("actualizar"), object: nil, userInfo: ["listo":array])
                
                print("se envio!!")
                
                
//                let jsonDict = JSON as! NSArray;
//                let weatherDict = jsonDict[2] as! NSDictionary
//                
//                r = weatherDict[0] as! String

            }
            
        }
        
    }
    
}
