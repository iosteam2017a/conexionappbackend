//
//  ArregloUsuarios.swift
//  FrontEnd
//
//  Created by Juan Erazo on 8/6/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import Foundation


class Persona {
    let nombre:String
    let apellido:String
    let cedula:String
    
    init(nombre:String,apellido:String,cedula:String){
        self.nombre = nombre
        self.apellido = apellido
        self.cedula = cedula
    }
}
