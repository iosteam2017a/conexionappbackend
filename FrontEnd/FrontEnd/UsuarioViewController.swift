//
//  UsuarioViewController.swift
//  FrontEnd
//
//  Created by Juan Erazo on 8/6/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class UsuarioViewController: UIViewController {
    let backend = BackEndManager()

    @IBOutlet weak var nombreTextField: UITextField!
    @IBOutlet weak var apellidoTextField: UITextField!
    @IBOutlet weak var cedulaTextField: UITextField!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func registrarButtonPressed(_ sender: Any) {
        print("sadasdasd")
        let url = "http://localhost:1337/usuario/create?nombre=\(nombreTextField.text!)&apellido=\(apellidoTextField.text!)&cedula=\(cedulaTextField.text!)"
        
        backend.solicitarBackEnd(urlString: url)
        limpiar()
    }

    
    func limpiar(){
        nombreTextField.text = ""
        apellidoTextField.text = ""
        cedulaTextField.text = ""
    }

}
